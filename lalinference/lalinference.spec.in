%define nightly @NIGHTLY_VERSION@%{nil}
%define _prefix /usr
%define _sysconfdir %{_prefix}/etc
%define release 1

%if "%{?nightly:%{nightly}}%{!?nightly:0}" == "%{nil}"
%undefine nightly
%endif

Name: @PACKAGE@
Version: @BASE_VERSION@
Release: %{?nightly:0.%{nightly}}%{!?nightly:%{release}}%{?dist}
Summary: LSC Algorithm Inference Library
License: GPLv2+
Group: LAL
Source: %{name}-%{version}%{?nightly:-%{nightly}}.tar.xz
URL: https://wiki.ligo.org/Computing/LALSuite
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildRequires: python2-rpm-macros
BuildRequires: pkgconfig >= 0.18.0
BuildRequires: chealpix-devel >= 3.30.0
BuildRequires: gsl-devel
BuildRequires: hdf5-devel
BuildRequires: healpy >= 1.9.1
BuildRequires: python-astropy >= 1.1.1
BuildRequires: python-matplotlib >= 1.2.0
BuildRequires: h5py
BuildRequires: python-shapely
BuildRequires: pytest
BuildRequires: libmetaio-devel
BuildRequires: numpy >= 1.7
BuildRequires: octave-devel
BuildRequires: python-devel
BuildRequires: scipy >= 0.9.0
BuildRequires: swig >= 3.0.7
BuildRequires: python2-gwdatafind
BuildRequires: python2-gwpy
BuildRequires: help2man
BuildRequires: glue >= @MIN_GLUE_VERSION@
BuildRequires: lal-devel >= @MIN_LAL_VERSION@
BuildRequires: lal-octave >= @MIN_LAL_VERSION@
BuildRequires: python2-lal >= @MIN_LAL_VERSION@
BuildRequires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@
BuildRequires: lalmetaio-octave >= @MIN_LALMETAIO_VERSION@
BuildRequires: python2-lalmetaio >= @MIN_LALMETAIO_VERSION@
BuildRequires: lalsimulation-devel >= @MIN_LALSIMULATION_VERSION@
BuildRequires: lalsimulation-octave >= @MIN_LALSIMULATION_VERSION@
BuildRequires: python2-lalsimulation >= @MIN_LALSIMULATION_VERSION@
BuildRequires: lalburst-devel >= @MIN_LALBURST_VERSION@
BuildRequires: lalburst-octave >= @MIN_LALBURST_VERSION@
BuildRequires: python2-lalburst >= @MIN_LALBURST_VERSION@
BuildRequires: lalinspiral-devel >= @MIN_LALINSPIRAL_VERSION@
BuildRequires: lalinspiral-octave >= @MIN_LALINSPIRAL_VERSION@
BuildRequires: python2-lalinspiral >= @MIN_LALINSPIRAL_VERSION@
BuildRequires: lalpulsar-devel >= @MIN_LALPULSAR_VERSION@
BuildRequires: lalpulsar-octave >= @MIN_LALPULSAR_VERSION@
BuildRequires: python2-lalpulsar >= @MIN_LALPULSAR_VERSION@
Requires: chealpix
Requires: gsl >= 1.15
Requires: hdf5
Requires: libmetaio
Requires: lal >= @MIN_LAL_VERSION@
Requires: lalmetaio >= @MIN_LALMETAIO_VERSION@
Requires: lalsimulation >= @MIN_LALSIMULATION_VERSION@
Requires: lalburst >= @MIN_LALBURST_VERSION@
Requires: lalinspiral >= @MIN_LALINSPIRAL_VERSION@
Requires: lalpulsar >= @MIN_LALPULSAR_VERSION@
Prefix: %{_prefix}

%description
The LSC Algorithm Inference Library for gravitational wave data analysis. This
package contains the shared-object libraries needed to run applications
that use the LAL Inference library.

%package devel
Summary: Files and documentation needed for compiling programs that use LAL Inference
Group: LAL
Requires: %{name} = %{version}
Requires: chealpix-devel >= 3.30.0
Requires: gsl-devel >= 1.15
Requires: libmetaio-devel
Requires: lal-devel >= @MIN_LAL_VERSION@
Requires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@
Requires: lalsimulation-devel >= @MIN_LALSIMULATION_VERSION@
Requires: lalburst-devel >= @MIN_LALBURST_VERSION@
Requires: lalinspiral-devel >= @MIN_LALINSPIRAL_VERSION@
Requires: lalpulsar-devel >= @MIN_LALPULSAR_VERSION@
%description devel
The LSC Algorithm Inference Library for gravitational wave data analysis. This
package contains files needed build applications that use the LAL Inference
library.

%package -n python2-%{name}
Summary: Python %{python2_version} Bindings for LALInference
Group: LAL
Requires: %{name} = %{version}
Requires: healpy >= 1.9.1
Requires: h5py
Requires: python-shapely
Requires: numpy >= 1.7
Requires: python
Requires: scipy >= 0.8
Requires: python-astropy >= 1.1.1
Requires: python2-gwdatafind
Requires: python-matplotlib >= 1.2.0
Requires: python-six
Requires: glue >= @MIN_GLUE_VERSION@
Requires: ligo-gracedb
Requires: python2-gwpy
Requires: python2-lal >= @MIN_LAL_VERSION@
Requires: python2-lalmetaio >= @MIN_LALMETAIO_VERSION@
Requires: python2-lalsimulation >= @MIN_LALSIMULATION_VERSION@
Requires: python2-lalburst >= @MIN_LALBURST_VERSION@
Requires: python2-lalinspiral >= @MIN_LALINSPIRAL_VERSION@
Requires: python2-lalpulsar >= @MIN_LALPULSAR_VERSION@
Obsoletes: %{name}-python
Provides: %{name}-python
%description -n python2-%{name}
The LSC Algorithm Library for gravitational wave data analysis.
This package provides the Python %{python2_version} bindings for LALInference.

%package octave
Summary: Octave Bindings for LALInference
Group: LAL
Requires: %{name} = %{version}
Requires: octave
Requires: lal-octave >= @MIN_LAL_VERSION@
Requires: lalmetaio-octave >= @MIN_LALMETAIO_VERSION@
Requires: lalsimulation-octave >= @MIN_LALSIMULATION_VERSION@
Requires: lalburst-octave >= @MIN_LALBURST_VERSION@
Requires: lalinspiral-octave >= @MIN_LALINSPIRAL_VERSION@
Requires: lalpulsar-octave >= @MIN_LALPULSAR_VERSION@
%description octave
The LSC Algorithm Library for gravitational wave data analysis.
This package provides the Octave bindings for LALInference.

%prep
%setup -q -n %{name}-%{version}%{?nightly:-%{nightly}}

%build
%configure --disable-gcc-flags --enable-swig --enable-openmp
%{__make} %{?_smp_mflags} V=1

%check
%{__make} %{?_smp_mflags} V=1 VERBOSE=1 check

%install
%make_install
find $RPM_BUILD_ROOT%{_libdir} -name '*.la' -delete

%post
ldconfig

%postun
ldconfig

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}%{?nightly:-%{nightly}}

%files
%defattr(-,root,root)
%license COPYING
%{_bindir}/lalinference_bench
%{_bindir}/lalinference_burst
%{_bindir}/lalinference_datadump
%{_bindir}/lalinference_injectedlike
%{_bindir}/lalinference_nest
%{_bindir}/lalinference_version
%{_bindir}/lalinference_mpi_wrapper
%{_datarootdir}/lalinference/*
%{_libdir}/*.so.*
%{_mandir}/man1/lalinference_bench.1*
%{_mandir}/man1/lalinference_burst.1*
%{_mandir}/man1/lalinference_datadump.1*
%{_mandir}/man1/lalinference_injectedlike.1*
%{_mandir}/man1/lalinference_nest.1*
%{_mandir}/man1/lalinference_version.1*
%{_sysconfdir}/*

%files devel
%defattr(-,root,root)
%license COPYING
%{_includedir}/lal
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%files -n python2-%{name}
%defattr(-,root,root)
%license COPYING
%{_bindir}/cbcBayes*
%{_bindir}/imrtgr_*
%{_bindir}/lalinference_burst_pp_pipe
%{_bindir}/lalinference_coherence_test
%{_bindir}/lalinference_compute_roq_weights
%{_bindir}/lalinference_merge_posteriors
%{_bindir}/lalinference_multi_pipe
%{_bindir}/lalinference_nest2pos
%{_bindir}/lalinference_cpnest
%{_bindir}/lalinference_pipe
%{_bindir}/lalinference_pp_pipe
%{_bindir}/lalinference_review_test
%{_bindir}/lalinference_tiger_pipe
%{_bindir}/rapidpe_*
%{_mandir}/man1/cbcBayes*.1*
%{_mandir}/man1/imrtgr_*.1*
%{_mandir}/man1/lalinference_burst_pp_pipe.1*
%{_mandir}/man1/lalinference_coherence_test.1*
%{_mandir}/man1/lalinference_compute_roq_weights.1*
%{_mandir}/man1/lalinference_merge_posteriors.1*
%{_mandir}/man1/lalinference_multi_pipe.1*
%{_mandir}/man1/lalinference_nest2pos.1*
%{_mandir}/man1/lalinference_cpnest.1*
%{_mandir}/man1/lalinference_pipe.1*
%{_mandir}/man1/lalinference_pp_pipe.1*
%{_mandir}/man1/lalinference_review_test.1*
%{_mandir}/man1/lalinference_tiger_pipe.1*
%{_mandir}/man1/rapidpe_*.1*
%{python2_sitearch}/*

%files octave
%defattr(-,root,root)
%license COPYING
%{_prefix}/lib*/octave/*/site/oct/*/lalinference.oct*

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Thu Jul 10 2019 Adam Mercer <adam.mercer@ligo.org> 1.11.2-1
- O3 point release

* Fri Jun 21 2019 Adam Mercer <adam.mercer@ligo.org> 1.11.1-1
- O3 point release

* Tue May 21 2019 Adam Mercer <adam.mercer@ligo.org> 1.11.0-1
- O3 point release

* Wed May 01 2019 Adam Mercer <adam.mercer@ligo.org> 1.10.3-1
- O3 point release

* Mon Feb 25 2019 Adam Mercer <adam.mercer@ligo.org> 1.10.2-1
- ER14 release

* Tue Jan 22 2019 Adam Mercer <adam.mercer@ligo.org> 1.10.1-1
- Fix spin2 prior

* Thu Sep 13 2018 Adam Mercer <adam.mercer@ligo.org> 1.10.0-1
- Pre O3 release

* Thu Jul 27 2017 Adam Mercer <adam.mercer@ligo.org> 1.9.4-1
- bayestar_plot_volume: work around broken fonts in SL7

* Wed Jul 26 2017 Adam Mercer <adam.mercer@ligo.org> 1.9.3-1
- BAYESTAR and sky map post-processing updates

* Thu Mar 16 2017 Adam Mercer <adam.mercer@ligo.org> 1.9.2-1
- lalinference.io.fits: remove some redundant keys on reading
- lalinference.io.fits: fix writing single-column sky maps

* Sat Feb 18 2017 Adam Mercer <adam.mercer@ligo.org> 1.9.1-1
- Do not use $(..._la_LIBADD) to link against external libraries
- Track version info API change
- Fix to weights calculation so that posterior samples are now drawn correctly
- Fix incorrect axes limits for Mollweide sky maps
- Drop code for reading past EOF of named pipe
- Fix incorrect reordering of IDL-compatible sky maps
- Tune default max distance for volume rendering plots

* Tue Feb 07 2017 Adam Mercer <adam.mercer@ligo.org> 1.9.0-1
- O2 release

* Thu Dec 15 2016 Adam Mercer <adam.mercer@ligo.org> 1.8.2-1
- Be more emphatic about switching matplotlib backend
- Prevent trying to build python3 packages when unsupported

* Fri Dec 09 2016 Adam Mercer <adam.mercer@ligo.org> 1.8.1-1
- Explicitly set no-op Matplotlib backend
- Set matplotlib backend to no-op

* Mon Sep 26 2016 Adam Mercer <adam.mercer@ligo.org> 1.8.0-1
- ER10 release

* Thu Jun 23 2016 Adam Mercer <adam.mercer@ligo.org> 1.7.0-1
- ER9 release

* Fri Mar 25 2016 Adam Mercer <adam.mercer@ligo.org> 1.6.0-1
- Pre O2 packaging test release
